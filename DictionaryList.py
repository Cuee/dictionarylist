class DictionaryList:
    '''
    Description: A dictionary with a list as its value type.
    '''
    _dictionary = None
    
    def __init__(self):
        self._dictionary = {}

    def __str__(self):
        return str(self._dictionary)

    def add(self, key, lst):
        '''
        Description: Adds a new pair to the dictionary list.
        key = The name of the key of the pair.
        lst = The list to be added to the dictionary.
        '''
        if list != type(lst):
            raise Exception(ErrorMessages.WRONG_ARGUMENT_TYPE)
        else:
            if not self.exists(key):
                self._dictionary[key] = lst
            elif self.exists(key):
                raise Exception(ErrorMessages.KEY_EXISTS)

    def join_lists(self, new_key, first_key, second_key):
        '''
        Description: Joins two lists in a dictionary list, returns the updated dictionary list.
        new_key = The name of the new key that is mapped to the resulting list after join.
        first_key = The key of the first list.
        second_key = The key of the second list.
        '''
        if list != type(self._dictionary[first_key]) and list != type(self._dictionary[second_key]):
            raise Exception(ErrorMessages.WRONG_ARGUMENT_TYPE)
        elif not self.exists(first_key) and not self.exists(second_key):
            raise Exception(ErrorMessages.NOT_A_KEY)
        else:
            new_list = self._dictionary[first_key] + self._dictionary[second_key]
            self.add(new_key, new_list)
            self.delete(first_key)
            self.delete(second_key)

    def append_to_list(self, key, value):
        '''
        Description: Appends a value to the specified list in the dictionary list, returns the updated dictionary list.
        key = The key of the list in the dictionary list.
        value = The value to be appended to the list in the dictionary list.
        '''
        if not self.exists(key):
            raise Exception(ErrorMessages.NOT_A_KEY)
        else:
            my_list = self._dictionary[key]
            my_list.append(value)

    def join(self, dictionary_list):
        '''
        Description: Concatenates two dictionary lists.
        '''
        if DictionaryList != type(dictionary_list):
            raise Exception(ErrorMessages.NOT_A_DICTIONARY_LIST)
        else:
            new_dict = dictionary_list._dictionary
            for key in new_dict:
                self.add(key, new_dict[key])

    def delete(self, key):
        '''
        Description: Deletes a key value pair in the dictionary list.
        key = The key of the pair to be deleted.
        '''
        if not self.exists(key):
            raise Exception(ErrorMessages.NOT_A_KEY)
        else:
            del(self._dictionary[key])
        
    def replace_key(self, old_key, new_key):
        '''
        Description: Replaces a key in the dictionary list.
        old_key = The current key of the pair.
        new_key = The new key of the pair.
        '''
        if not self.exists(old_key) and not self.exists(new_key):
            raise Exception(ErrorMessages.NOT_A_KEY)
        else:
            lst = self._dictionary[old_key]
            self.add(new_key, lst)
            self.delete(old_key)
           
    def replace_list(self, key, new_list):
        '''
        Description: Replaces a list in the dictionary list.
        key = The current key of the pair.
        new_list = The new list of the pair.
        '''
        if not self.exists(key):
            raise Exception(ErrorMessages.NOT_A_KEY)
        else:
            self._dictionary[key] = new_list

    def search(self, key):
        '''
        Description: Searches the dictionary list for the pair mapped to this key.
        key = The key mapped to the list.
        '''
        if not self.exists(key):
            raise Exception(ErrorMessages.NOT_A_KEY)
        else:
            return self._dictionary[key]

    def length(self):
        '''
        Description: The length of the dictionary list.
        '''
        return len(self._dictionary)
             
    def exists(self, key):
        '''
        Description: Checks if a key exists in the dictionary list.
        key = The key that to be sought.
        '''
        if key in self._dictionary:
            return True
        else:
            return False

        
class ErrorMessages:

    WRONG_ARGUMENT_TYPE = "Unexpected argument type."
    NOT_A_DICTIONARY_LIST = "Dictionary list has not been initialized."
    NOT_A_KEY = "Invalid element in dictionary list."
    KEY_EXISTS = "An element with this key exists."

    
